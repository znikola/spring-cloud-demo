package com.example;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@EnableDiscoveryClient
public class NesServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(NesServiceApplication.class, args);
	}
	
}

@RestController
class NesController {
	
	private static final Logger LOGGER = LogManager.getLogger(NesController.class);
	
	@GetMapping("/")
	String home() {
		return "NES";
	}
	
	@GetMapping("/export")
	String export() {
		LOGGER.info("Exporting data...");
		return "Data exported";
	}
	
}