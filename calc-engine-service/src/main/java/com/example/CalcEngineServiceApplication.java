package com.example;

import java.util.Objects;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableDiscoveryClient
public class CalcEngineServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CalcEngineServiceApplication.class, args);
	}
	
	@Bean
	@LoadBalanced
	RestTemplate restTemplate() {
		return new RestTemplate();
	}
	
}

@RefreshScope
@RestController
class ConfigTestController {
	
	private final String message;
	
	ConfigTestController(@Value("${ce.message}") final String message) {
		this.message = Objects.requireNonNull(message);
	}
	
	@GetMapping("/test-message")
	String readMessage() {
		return message;
	}
	
}

@RestController
class CalcEngineController {
	
	private static final Logger LOGGER = LogManager.getLogger(CalcEngineController.class);
	
	private final RestTemplate restTemplate;
	
	CalcEngineController(@Lazy final RestTemplate restTemplate) {
		this.restTemplate = Objects.requireNonNull(restTemplate);
	}
	
	@GetMapping("/")
	String home() {
		return "calc-engine";
	}
	
	@GetMapping("/calculate")
	int calculate() {
		LOGGER.info("Calculating...");
		return 69;
	}
	
	@GetMapping("/call-nes")
	String callNes() {
		return restTemplate.getForObject("http://nes-service/export", String.class);
	}
	
}