package com.example;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.netflix.loadbalancer.AvailabilityFilteringRule;
import com.netflix.loadbalancer.IPing;
import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.PingUrl;

/**
 * <b>WARNING</b>: The NesRibbonConfig has to be @Configuration but take care that it is not in a @ComponentScan for the main application context,
 * otherwise it will be shared by all the @RibbonClients. If you use @ComponentScan (or @SpringBootApplication) you need to take steps to avoid it being included
 * (for instance put it in a separate, non-overlapping package, or specify the packages to scan explicitly in the @ComponentScan).

 * @author nikola.zaric
 *
 */
@Configuration
class NeoRibbonClient {

	@Bean
	IPing ribbonPing() {
		return new PingUrl();
	}
	@Bean
	IRule ribbonRule() {
		return new AvailabilityFilteringRule();
	}
	
}
