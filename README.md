calc-engine-service requires a local git repository located at ${user.home}\Desktop\neo-config-repo and this is configured in application.yml file inside of config-server project.
You can create two files there: application.yml and calc-engine-service.yml:
application.yml:
logging: 
  level: 
    org.springframework: INFO
    
calc-engine-service.yml:
server: 
  port: 9091
  
ce: 
  message: test message
  
After this do a git init and commit everything