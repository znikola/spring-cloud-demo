package com.example;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

@EnableCircuitBreaker
@EnableZuulProxy
@SpringBootApplication
@RibbonClient(name = "NEO - Ribbon", configuration = NeoRibbonClient.class)
public class NeoGatewayServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(NeoGatewayServiceApplication.class, args);
	}

	@Bean
	@LoadBalanced
	RestTemplate restTemplate() {
		return new RestTemplate();
	}
	
}

@RestController
class NeoGatewayController {

	private final RestTemplate restTemplate;
	
	@Autowired
	NeoGatewayController(@Lazy final RestTemplate restTemplate) {
		this.restTemplate = Objects.requireNonNull(restTemplate);
	}
	
	@HystrixCommand(fallbackMethod = "backupMethod")
	@GetMapping("/test-fail")
	Integer failCall() {
		return restTemplate.getForObject("http://calc-engine-service/calculate", Integer.class);
	}
	
	Integer backupMethod() {
		return 100;
	}
	
}